﻿namespace Gimpies
{
    partial class mainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainMenu));
            this.loginBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stockBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.stockBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.loginBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.view_csv = new System.Windows.Forms.DataGridView();
            this.stockBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.txt_Filepath = new ChreneLib.Controls.TextBoxes.CTextBox();
            this.btn_load = new System.Windows.Forms.Button();
            this.btn_export_data = new System.Windows.Forms.Button();
            this.stockBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.list_output = new System.Windows.Forms.ListBox();
            this.btn_del = new System.Windows.Forms.Button();
            this.btn_row_add = new System.Windows.Forms.Button();
            this.lbl_view_csv = new System.Windows.Forms.Label();
            this.lbl_output = new System.Windows.Forms.Label();
            this.btn_save_file = new System.Windows.Forms.Button();
            this.txt_permssion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_logout = new System.Windows.Forms.Button();
            this.btn_open_stock = new System.Windows.Forms.Button();
            this.btn_backup = new System.Windows.Forms.Button();
            this.btn_poweroff = new System.Windows.Forms.Button();
            this.btn_inkoop = new System.Windows.Forms.Button();
            this.tooltip_load_file = new System.Windows.Forms.ToolTip(this.components);
            this.tip_btn_export = new System.Windows.Forms.ToolTip(this.components);
            this.row_add_tip = new System.Windows.Forms.ToolTip(this.components);
            this.backup_stock_tip = new System.Windows.Forms.ToolTip(this.components);
            this.tip_del_select = new System.Windows.Forms.ToolTip(this.components);
            this.tip_sve_btn = new System.Windows.Forms.ToolTip(this.components);
            this.tip_rld_euro = new System.Windows.Forms.ToolTip(this.components);
            this.btn_reload_euros = new System.Windows.Forms.Button();
            this.btn_verkoop_schoen = new System.Windows.Forms.Button();
            this.lbl_euro = new System.Windows.Forms.Label();
            this.num_inkoop = new System.Windows.Forms.NumericUpDown();
            this.num_verkoop = new System.Windows.Forms.NumericUpDown();
            this.tip_logout = new System.Windows.Forms.ToolTip(this.components);
            this.tip_exit = new System.Windows.Forms.ToolTip(this.components);
            this.tip_open_stock = new System.Windows.Forms.ToolTip(this.components);
            this.tip_inkoop_schoen = new System.Windows.Forms.ToolTip(this.components);
            this.tip_verkoop_schoen = new System.Windows.Forms.ToolTip(this.components);
            this.tip_list_output = new System.Windows.Forms.ToolTip(this.components);
            this.tip_datagrid = new System.Windows.Forms.ToolTip(this.components);
            this.btn_reset_artikelen = new System.Windows.Forms.Button();
            this.btn_reset_euros = new System.Windows.Forms.Button();
            this.tip_rst_art = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.view_csv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_inkoop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_verkoop)).BeginInit();
            this.SuspendLayout();
            // 
            // view_csv
            // 
            this.view_csv.AllowUserToAddRows = false;
            this.view_csv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.view_csv.Location = new System.Drawing.Point(12, 12);
            this.view_csv.Name = "view_csv";
            this.view_csv.Size = new System.Drawing.Size(658, 371);
            this.view_csv.TabIndex = 32;
            this.tip_datagrid.SetToolTip(this.view_csv, "Hier komt de data van het datagrid");
            // 
            // txt_Filepath
            // 
            this.txt_Filepath.Location = new System.Drawing.Point(870, 12);
            this.txt_Filepath.Name = "txt_Filepath";
            this.txt_Filepath.Size = new System.Drawing.Size(188, 20);
            this.txt_Filepath.TabIndex = 34;
            this.txt_Filepath.WaterMark = "Filepath";
            this.txt_Filepath.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txt_Filepath.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Filepath.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // btn_load
            // 
            this.btn_load.Location = new System.Drawing.Point(870, 50);
            this.btn_load.Name = "btn_load";
            this.btn_load.Size = new System.Drawing.Size(86, 23);
            this.btn_load.TabIndex = 37;
            this.btn_load.Text = "Load file";
            this.tooltip_load_file.SetToolTip(this.btn_load, "Deze button is om bestanden in het vak te laden");
            this.btn_load.UseVisualStyleBackColor = true;
            this.btn_load.Click += new System.EventHandler(this.btn_load_file_Click);
            // 
            // btn_export_data
            // 
            this.btn_export_data.Location = new System.Drawing.Point(962, 50);
            this.btn_export_data.Name = "btn_export_data";
            this.btn_export_data.Size = new System.Drawing.Size(96, 23);
            this.btn_export_data.TabIndex = 38;
            this.btn_export_data.Text = "Export Data";
            this.tip_btn_export.SetToolTip(this.btn_export_data, "Hier mee kun je de data van de datagrid naar de list exporteren");
            this.btn_export_data.UseVisualStyleBackColor = true;
            this.btn_export_data.Click += new System.EventHandler(this.btn_save_file_Click);
            // 
            // list_output
            // 
            this.list_output.FormattingEnabled = true;
            this.list_output.Location = new System.Drawing.Point(12, 418);
            this.list_output.MultiColumn = true;
            this.list_output.Name = "list_output";
            this.list_output.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.list_output.Size = new System.Drawing.Size(623, 199);
            this.list_output.TabIndex = 39;
            this.tip_list_output.SetToolTip(this.list_output, "Klik een keer met je linker muisknop om het te kopieren");
            this.list_output.SelectedIndexChanged += new System.EventHandler(this.list_output_SelectedIndexChanged);
            // 
            // btn_del
            // 
            this.btn_del.Location = new System.Drawing.Point(676, 12);
            this.btn_del.Name = "btn_del";
            this.btn_del.Size = new System.Drawing.Size(100, 23);
            this.btn_del.TabIndex = 40;
            this.btn_del.Text = "Delete Selected";
            this.tip_del_select.SetToolTip(this.btn_del, "Hiermee kun je een regel weghalen");
            this.btn_del.UseVisualStyleBackColor = true;
            this.btn_del.Click += new System.EventHandler(this.btn_del_Click);
            // 
            // btn_row_add
            // 
            this.btn_row_add.Location = new System.Drawing.Point(676, 50);
            this.btn_row_add.Name = "btn_row_add";
            this.btn_row_add.Size = new System.Drawing.Size(100, 23);
            this.btn_row_add.TabIndex = 41;
            this.btn_row_add.Text = "Row Add";
            this.tip_rld_euro.SetToolTip(this.btn_row_add, "Hiermee kun je het geld opnieuw laden");
            this.row_add_tip.SetToolTip(this.btn_row_add, "Hier mee kun je een regel toevoegen");
            this.btn_row_add.UseVisualStyleBackColor = true;
            this.btn_row_add.Click += new System.EventHandler(this.btn_row_add_Click);
            // 
            // lbl_view_csv
            // 
            this.lbl_view_csv.AutoSize = true;
            this.lbl_view_csv.Location = new System.Drawing.Point(676, 79);
            this.lbl_view_csv.Name = "lbl_view_csv";
            this.lbl_view_csv.Size = new System.Drawing.Size(143, 13);
            this.lbl_view_csv.TabIndex = 42;
            this.lbl_view_csv.Text = "Lege regel komen onder aan";
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Location = new System.Drawing.Point(12, 620);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(409, 13);
            this.lbl_output.TabIndex = 43;
            this.lbl_output.Text = "Je kunt de data kopiëren uit het kader door dubbel te klikken na dat de data expo" +
    "rt is";
            // 
            // btn_save_file
            // 
            this.btn_save_file.Location = new System.Drawing.Point(962, 79);
            this.btn_save_file.Name = "btn_save_file";
            this.btn_save_file.Size = new System.Drawing.Size(96, 23);
            this.btn_save_file.TabIndex = 44;
            this.btn_save_file.Text = "Save CSV FIle";
            this.tip_sve_btn.SetToolTip(this.btn_save_file, "Hiermee kun je de list data opslaan");
            this.btn_save_file.UseVisualStyleBackColor = true;
            this.btn_save_file.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // txt_permssion
            // 
            this.txt_permssion.Location = new System.Drawing.Point(881, 568);
            this.txt_permssion.Name = "txt_permssion";
            this.txt_permssion.Size = new System.Drawing.Size(100, 20);
            this.txt_permssion.TabIndex = 45;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(676, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "LAAT NOOIT EEN WIT REGEL IN JE CSV FILE";
            // 
            // btn_logout
            // 
            this.btn_logout.Location = new System.Drawing.Point(983, 620);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(75, 23);
            this.btn_logout.TabIndex = 47;
            this.btn_logout.Text = "Logout";
            this.tip_logout.SetToolTip(this.btn_logout, "Hiermee kun je uitloggen");
            this.btn_logout.UseVisualStyleBackColor = true;
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // btn_open_stock
            // 
            this.btn_open_stock.Location = new System.Drawing.Point(825, 620);
            this.btn_open_stock.Name = "btn_open_stock";
            this.btn_open_stock.Size = new System.Drawing.Size(152, 23);
            this.btn_open_stock.TabIndex = 49;
            this.btn_open_stock.Text = "Open Stock File (Stock.csv)";
            this.tip_open_stock.SetToolTip(this.btn_open_stock, "Hiermee kun je het stock bestand openen");
            this.btn_open_stock.UseVisualStyleBackColor = true;
            this.btn_open_stock.Click += new System.EventHandler(this.btn_open_stock_Click);
            // 
            // btn_backup
            // 
            this.btn_backup.Location = new System.Drawing.Point(870, 79);
            this.btn_backup.Name = "btn_backup";
            this.btn_backup.Size = new System.Drawing.Size(86, 23);
            this.btn_backup.TabIndex = 50;
            this.btn_backup.Text = "Backup Stock";
            this.backup_stock_tip.SetToolTip(this.btn_backup, "hier mee kun je een backup maken van je stock");
            this.btn_backup.UseVisualStyleBackColor = true;
            this.btn_backup.Click += new System.EventHandler(this.btn_backup_Click);
            // 
            // btn_poweroff
            // 
            this.btn_poweroff.Location = new System.Drawing.Point(983, 594);
            this.btn_poweroff.Name = "btn_poweroff";
            this.btn_poweroff.Size = new System.Drawing.Size(75, 23);
            this.btn_poweroff.TabIndex = 51;
            this.btn_poweroff.Text = "Exit";
            this.tip_exit.SetToolTip(this.btn_poweroff, "Hiermee kun je het programma afsluiten");
            this.btn_poweroff.UseVisualStyleBackColor = true;
            this.btn_poweroff.Click += new System.EventHandler(this.btn_poweroff_Click);
            // 
            // btn_inkoop
            // 
            this.btn_inkoop.Location = new System.Drawing.Point(676, 171);
            this.btn_inkoop.Name = "btn_inkoop";
            this.btn_inkoop.Size = new System.Drawing.Size(100, 23);
            this.btn_inkoop.TabIndex = 52;
            this.btn_inkoop.Text = "Inkoop Schoen";
            this.tip_inkoop_schoen.SetToolTip(this.btn_inkoop, "Hiermee kun je schoenen verkopen");
            this.btn_inkoop.UseVisualStyleBackColor = true;
            this.btn_inkoop.Click += new System.EventHandler(this.btn_inkoop_Click);
            // 
            // btn_reload_euros
            // 
            this.btn_reload_euros.Location = new System.Drawing.Point(962, 108);
            this.btn_reload_euros.Name = "btn_reload_euros";
            this.btn_reload_euros.Size = new System.Drawing.Size(96, 23);
            this.btn_reload_euros.TabIndex = 58;
            this.btn_reload_euros.Text = "Reload Euro\'s";
            this.tip_rld_euro.SetToolTip(this.btn_reload_euros, "Hiermee kun je het geld reloaden");
            this.btn_reload_euros.UseVisualStyleBackColor = true;
            this.btn_reload_euros.Click += new System.EventHandler(this.btn_reload_euros_Click);
            // 
            // btn_verkoop_schoen
            // 
            this.btn_verkoop_schoen.Location = new System.Drawing.Point(676, 200);
            this.btn_verkoop_schoen.Name = "btn_verkoop_schoen";
            this.btn_verkoop_schoen.Size = new System.Drawing.Size(100, 23);
            this.btn_verkoop_schoen.TabIndex = 53;
            this.btn_verkoop_schoen.Text = "Verkoop Schoen";
            this.tip_verkoop_schoen.SetToolTip(this.btn_verkoop_schoen, "hiermee kun je schoenen verkopen");
            this.btn_verkoop_schoen.UseVisualStyleBackColor = true;
            this.btn_verkoop_schoen.Click += new System.EventHandler(this.btn_verkoop_schoen_Click);
            // 
            // lbl_euro
            // 
            this.lbl_euro.AutoSize = true;
            this.lbl_euro.Location = new System.Drawing.Point(684, 625);
            this.lbl_euro.Name = "lbl_euro";
            this.lbl_euro.Size = new System.Drawing.Size(0, 13);
            this.lbl_euro.TabIndex = 55;
            // 
            // num_inkoop
            // 
            this.num_inkoop.Location = new System.Drawing.Point(782, 174);
            this.num_inkoop.Name = "num_inkoop";
            this.num_inkoop.Size = new System.Drawing.Size(120, 20);
            this.num_inkoop.TabIndex = 56;
            // 
            // num_verkoop
            // 
            this.num_verkoop.Location = new System.Drawing.Point(782, 203);
            this.num_verkoop.Name = "num_verkoop";
            this.num_verkoop.Size = new System.Drawing.Size(120, 20);
            this.num_verkoop.TabIndex = 57;
            // 
            // btn_reset_artikelen
            // 
            this.btn_reset_artikelen.Location = new System.Drawing.Point(881, 594);
            this.btn_reset_artikelen.Name = "btn_reset_artikelen";
            this.btn_reset_artikelen.Size = new System.Drawing.Size(96, 23);
            this.btn_reset_artikelen.TabIndex = 59;
            this.btn_reset_artikelen.Text = "Reset Artikelen";
            this.tip_rst_art.SetToolTip(this.btn_reset_artikelen, "Hiermee kun je artikelen resetten");
            this.btn_reset_artikelen.UseVisualStyleBackColor = true;
            this.btn_reset_artikelen.Click += new System.EventHandler(this.btn_reset_artikelen_Click);
            // 
            // btn_reset_euros
            // 
            this.btn_reset_euros.Location = new System.Drawing.Point(983, 565);
            this.btn_reset_euros.Name = "btn_reset_euros";
            this.btn_reset_euros.Size = new System.Drawing.Size(75, 23);
            this.btn_reset_euros.TabIndex = 60;
            this.btn_reset_euros.Text = "Reset Euro\'s";
            this.tip_exit.SetToolTip(this.btn_reset_euros, "Hiermee kun je het programma afsluiten");
            this.btn_reset_euros.UseVisualStyleBackColor = true;
            this.btn_reset_euros.Click += new System.EventHandler(this.btn_reset_euros_Click);
            // 
            // mainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 648);
            this.Controls.Add(this.btn_reset_euros);
            this.Controls.Add(this.btn_reset_artikelen);
            this.Controls.Add(this.btn_reload_euros);
            this.Controls.Add(this.num_verkoop);
            this.Controls.Add(this.num_inkoop);
            this.Controls.Add(this.lbl_euro);
            this.Controls.Add(this.btn_verkoop_schoen);
            this.Controls.Add(this.btn_inkoop);
            this.Controls.Add(this.btn_poweroff);
            this.Controls.Add(this.btn_backup);
            this.Controls.Add(this.btn_open_stock);
            this.Controls.Add(this.btn_logout);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_permssion);
            this.Controls.Add(this.btn_save_file);
            this.Controls.Add(this.lbl_output);
            this.Controls.Add(this.lbl_view_csv);
            this.Controls.Add(this.btn_row_add);
            this.Controls.Add(this.btn_del);
            this.Controls.Add(this.list_output);
            this.Controls.Add(this.btn_export_data);
            this.Controls.Add(this.btn_load);
            this.Controls.Add(this.txt_Filepath);
            this.Controls.Add(this.view_csv);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "mainMenu";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Gimpies Programma";
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.view_csv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_inkoop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_verkoop)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource stockBindingSource;
        private System.Windows.Forms.BindingSource loginBindingSource;
        private System.Windows.Forms.BindingSource stockBindingSource1;
        private System.Windows.Forms.BindingSource loginBindingSource1;
        private System.Windows.Forms.DataGridView view_csv;
        private System.Windows.Forms.BindingSource stockBindingSource2;
        private ChreneLib.Controls.TextBoxes.CTextBox txt_Filepath;
        private System.Windows.Forms.BindingSource stockBindingSource3;
        private System.Windows.Forms.Button btn_load;
        private System.Windows.Forms.Button btn_export_data;
        private System.Windows.Forms.ListBox list_output;
        private System.Windows.Forms.Button btn_del;
        private System.Windows.Forms.Button btn_row_add;
        private System.Windows.Forms.Label lbl_view_csv;
        private System.Windows.Forms.Label lbl_output;
        private System.Windows.Forms.Button btn_save_file;
        private System.Windows.Forms.TextBox txt_permssion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_logout;
        private System.Windows.Forms.Button btn_open_stock;
        private System.Windows.Forms.Button btn_backup;
        private System.Windows.Forms.Button btn_poweroff;
        private System.Windows.Forms.Button btn_inkoop;
        private System.Windows.Forms.ToolTip tooltip_load_file;
        private System.Windows.Forms.ToolTip tip_btn_export;
        private System.Windows.Forms.ToolTip row_add_tip;
        private System.Windows.Forms.ToolTip backup_stock_tip;
        private System.Windows.Forms.ToolTip tip_del_select;
        private System.Windows.Forms.ToolTip tip_sve_btn;
        private System.Windows.Forms.ToolTip tip_rld_euro;
        private System.Windows.Forms.Button btn_verkoop_schoen;
        private System.Windows.Forms.Label lbl_euro;
        private System.Windows.Forms.NumericUpDown num_inkoop;
        private System.Windows.Forms.NumericUpDown num_verkoop;
        private System.Windows.Forms.Button btn_reload_euros;
        private System.Windows.Forms.ToolTip tip_inkoop_schoen;
        private System.Windows.Forms.ToolTip tip_verkoop_schoen;
        private System.Windows.Forms.ToolTip tip_logout;
        private System.Windows.Forms.ToolTip tip_exit;
        private System.Windows.Forms.ToolTip tip_open_stock;
        private System.Windows.Forms.ToolTip tip_list_output;
        private System.Windows.Forms.ToolTip tip_datagrid;
        private System.Windows.Forms.Button btn_reset_artikelen;
        private System.Windows.Forms.Button btn_reset_euros;
        private System.Windows.Forms.ToolTip tip_rst_art;
    }
}

