﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;


namespace Gimpies
{
    public partial class loginScreen : Form
    {

        public loginScreen()
        {
            string login = @"login.csv";
            InitializeComponent();
            if (!File.Exists(login))
            {


                btn_first.Visible = true;
                MessageBox.Show(" Omdat dit u eerste keer is klik op OK", "BELANGRIJK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Thread.Sleep(700);
                MessageBox.Show("Klik daarna op Eerste keer", "BELANGRIJK", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            else
            {
                btn_first.Visible = false;


            
            }
        }
    
        //login buttin event on click
        //---------------------------------------------------------------------------------------------------------------------------------
        private void btn_login_Click(object sender, EventArgs e)
        {
            string role = null;
            bool found = false;
            //gives reads login and permissions 
            StreamReader sr = new StreamReader(@"login.csv");
            var lines = new List<string[]>();
            while (!sr.EndOfStream)
            {
                string[] line = sr.ReadLine().Split(',');
                lines.Add(line);

            }
            sr.Close();
            for (int i = 0; i < lines.Count; i++)
            {
                //checks for login and permissions
                if (txt_username.Text == lines[i][0] && txt_password.Text == lines[i][1] && lines[i][2] == "admin")
                {
                    MessageBox.Show("Je bent ingelogd!", "Succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    found = true;
                    role = "admin";
                    mainMenu mainMenu = new mainMenu(role);
                    mainMenu.Show();
                    this.Hide();
                    break;
                }
                //permissions manager
                else if (txt_username.Text == lines[i][0] && txt_password.Text == lines[i][1] && lines[i][2] == "manager")
                {
                    MessageBox.Show("Je bent ingelogd!", "Succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    found = true;
                    role = "manager";
                    mainMenu mainMenu = new mainMenu(role);
                    mainMenu.Show();
                    this.Hide();
                    break;
                }
                //permissions verkoper
                else if (txt_username.Text == lines[i][0] && txt_password.Text == lines[i][1] && lines[i][2] == "verkoper")
                {
                    MessageBox.Show("Je bent ingelogd!", "Succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    found = true;
                    role = "verkoper";
                    mainMenu mainMenu = new mainMenu(role);
                    mainMenu.Show();
                    this.Hide();
                    break;

                }

            }
            //check gives error code
            if (found == false)
            {
                MessageBox.Show("Verkeerde inlog", "Niet gelukt", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            found = false;

        }

        private void button1_Click(object sender, EventArgs e)

        {
            Directory.CreateDirectory("stock");
            Directory.CreateDirectory("Backup");
            Directory.CreateDirectory("Verkocht");
            StreamWriter st = new StreamWriter(@"login.csv");
            StreamWriter stocck = new StreamWriter(@"stock\Stock.csv");
            StreamWriter bat = new StreamWriter(@"shutdown.bat");
            bat.WriteLine("taskkill /IM Gimpies.exe");
            st.WriteLine("manager,manager,manager");
            stocck.WriteLine("");
            bat.Close();
            st.Close();
            stocck.Close();
            MessageBox.Show("Bestanden maken succesvol", "Succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Thread.Sleep(500);
            MessageBox.Show("login met USERNAME:manager PASSWORD:manager reset het daarna", "Bestand geladen", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Thread.Sleep(500);
            Process.Start(@"Gimpies.exe");
            this.Close();

        }
    }
}
