﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Diagnostics;

namespace Gimpies
{
    public partial class mainMenu : Form
    {
        string CSVFILE = "";


        public mainMenu(string role)
        {

            InitializeComponent();
            txt_permssion.Visible = false;

            // hier checkt de software welke permissions je hebt.
            // en zet dat in een textbox
            if (role == "admin")
            {
                txt_permssion.Text = "admin";
            }
            else if (role == "manager")
            {
                txt_permssion.Text = "manager";

            }
            else if (role == "verkoper")
            {
                txt_permssion.Text = "verkoper";

            }
        }

        private void euroload()
        {
            List<int> euros = new List<int>();
            StreamReader sr = new StreamReader(@"Verkocht\euros.txt");
            while (!sr.EndOfStream)
            {
                //, weghalen
                string[] line = sr.ReadLine().Split();
                int euro = Convert.ToInt32(line[0]);
                euros.Add(euro);


            }
            sr.Close();
            int totaal = euros.Sum();
            lbl_euro.Text = " totaal euros = €" + totaal.ToString();
        }
        private void Savecsv()
        {
            list_output.Items.Clear();
            string[,] data = new string[view_csv.RowCount, view_csv.ColumnCount];
            for (int row = 0; row < view_csv.RowCount; row++)
            {
                for (int column = 0; column < view_csv.ColumnCount; column++)
                {
                    data[row, column] = view_csv.Rows[row].Cells[column].Value.ToString();
                }
            }

            int i = 1;
            string rowtekentje = "";
            foreach (string x in data)
            {
                if (i == view_csv.Columns.Count)
                {
                    rowtekentje += x;
                    list_output.Items.Add(rowtekentje);
                    rowtekentje = "";
                    i = 1;
                }
                else
                {
                    i++;
                    rowtekentje += x + ",";
                }
            }


        }

        private void SaveBackupcsvFile()
        {
            if (txt_Filepath.Text.Contains("login"))
            {
                if (txt_permssion.Text == "manager")
                {
                    string filepath = @"login.csv";
                    StreamWriter st = new StreamWriter(filepath);
                    foreach (var item in list_output.Items)
                    {
                        st.WriteLine(item);
                    }
                    st.Close();
                    MessageBox.Show("je bestand is opgeslagen met de naam: " + filepath, "succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                string[] woorden = new string[2] { "BACKUP", "Stock" };
                for (int i = 0; i < woorden.Count(); i++)

                {
                    if (txt_Filepath.Text.Contains(woorden[i]))
                    {
                        string filepath = @"Backup/BACKUP_STOCK" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".csv";
                        StreamWriter st = new StreamWriter(filepath);
                        foreach (var item in list_output.Items)
                        {
                            st.WriteLine(item);
                        }
                        st.Close();
                        MessageBox.Show("je bestand is opgeslagen met de naam: " + "BACKUP_STOCK" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".csv", "succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void SavecsvFile()
        {
            if (txt_Filepath.Text.Contains("login"))
            {
                if (txt_permssion.Text == "manager")
                {
                    string filepath = @"login.csv";
                    StreamWriter st = new StreamWriter(filepath);
                    foreach (var item in list_output.Items)
                    {
                        st.WriteLine(item);
                    }
                    st.Close();
                    MessageBox.Show("je bestand is opgeslagen met de naam: " + filepath, "succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                string[] woorden = new string[2] { "BACKUP", "Stock" };
                for (int i = 0; i < woorden.Count(); i++)

                {
                    if (txt_Filepath.Text.Contains(woorden[i]))
                    {
                        string filepath = @"stock/Stock.csv";
                        StreamWriter st = new StreamWriter(filepath);
                        foreach (var item in list_output.Items)
                        {
                            st.WriteLine(item);
                        }
                        st.Close();
                        MessageBox.Show("je bestand is opgeslagen met de naam: " + "Stock.csv", "succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
        }

        private void Readcsv()
        {
            //opent select venster
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "select CSV File";
            ofd.Filter = "CSV |*.csv";
            string file = null;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //csv readen
                file = ofd.FileName;
                txt_Filepath.Text = ofd.FileName;
                StreamReader sr = new StreamReader(file);
                var lines = new List<string[]>();
                while (!sr.EndOfStream)
                {
                    //, weghalen
                    string[] line = sr.ReadLine().Split(',');
                    lines.Add(line);
                }
                sr.Close();
                //regels aan datagrid toevoegen
                if (ofd.SafeFileName == @"login.csv")
                {
                    if (txt_permssion.Text == "manager")
                    {
                        btn_inkoop.Visible = false;
                        btn_verkoop_schoen.Visible = false;
                        num_inkoop.Visible = false;
                        num_verkoop.Visible = false;
                        view_csv.Columns.Clear();
                        view_csv.Rows.Clear();
                        view_csv.Columns.Add("username", "Username");
                        view_csv.Columns.Add("password ", "Password");
                        view_csv.Columns.Add("permissions", "Permissions");
                        for (int i = 0; i < lines.Count(); i++)
                        {
                            view_csv.Rows.Add(lines[i]);
                        }
                    }
                    else
                    {
                        if (txt_permssion.Text == "verkoper")
                        {
                            num_inkoop.Visible = false;
                            num_verkoop.Visible = false;
                            btn_inkoop.Visible = false;
                            btn_verkoop_schoen.Visible = false;
                            MessageBox.Show("je hebt niet de goede permissies", "Niet succesvol", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                }
                else
                {
                    btn_inkoop.Visible = true;
                    btn_verkoop_schoen.Visible = true;
                    num_inkoop.Visible = true;
                    num_verkoop.Visible = true;
                    view_csv.Columns.Clear();
                    view_csv.Rows.Clear();
                    view_csv.Columns.Add("merk", "Merk");
                    view_csv.Columns.Add("soort ", "Soort");
                    view_csv.Columns.Add("maat", "Maat");
                    view_csv.Columns.Add("euro", "Euro");
                    view_csv.Columns.Add("kleur", "Kleur");
                    view_csv.Columns.Add("aantal", "Aantal");
                    view_csv.Columns.Add("id", "ID");
                    for (int i = 0; i < lines.Count(); i++)
                    {
                        view_csv.Rows.Add(lines[i]);
                    }
                }
            }
        }

        private void btn_load_file_Click(object sender, EventArgs e)
        {
            Readcsv();

        }

        private void btn_save_file_Click(object sender, EventArgs e)
        {
            Savecsv();
        }

        private void list_output_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                Clipboard.SetText(string.Join(Environment.NewLine, list_output.Items.OfType<string>()));
                MessageBox.Show("Gekopieerd", "Gekopieerd", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btn_del_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in view_csv.SelectedRows)
            {
                view_csv.Rows.RemoveAt(row.Index);
            }
            MessageBox.Show("Selectie is weg gehaald", "Succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btn_row_add_Click(object sender, EventArgs e)
        {
            view_csv.Rows.Add("", "", "", "", "");
            MessageBox.Show("Nieuwe lege regel toegevoegt", "Succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information); 

        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            SavecsvFile();
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            Process.Start("shutdown.bat");
            Thread.Sleep(50);
            Process.Start(@"Gimpies.exe");
            this.Close();
        }

        private void btn_open_save_Click(object sender, EventArgs e)
        {
            if (File.Exists(CSVFILE))
            {
                Process.Start(CSVFILE);
            }
            else
            {
                Process.Start(@"Stock.csv");
            }
        }

        private void btn_backup_Click(object sender, EventArgs e)
        {
            SaveBackupcsvFile();
        }

        private void btn_open_stock_Click(object sender, EventArgs e)
        {
            Process.Start(@"stock\Stock.csv");
        }

        private void btn_poweroff_Click(object sender, EventArgs e)
        {
            Process.Start("shutdown.bat");
        }

        private void btn_inkoop_Click(object sender, EventArgs e)
        {
            int selectedrowindex = view_csv.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = view_csv.Rows[selectedrowindex];
            int selected = Convert.ToInt32(selectedRow.Cells["aantal"].Value);
            int schoen = Convert.ToInt32(num_inkoop.Value);
            int alles = selected + schoen;
            int me = selected - schoen;
            object obj = alles;
            selectedRow.Cells["aantal"].Value = obj;
            //euros verwijderen
            string zoeken = Convert.ToString(selectedRow.Cells["euro"].Value);
            string tempFile = Path.GetTempFileName();
            string s = "";
            int count = 0;
            int alless = 0;
            using (StreamReader srr = new StreamReader(@"Verkocht\euros.txt"))
            {
                while ((s = srr.ReadLine()) != null)
                {
                    count++;
                }
            }
            alless = count - schoen;
            StreamReader sr = new StreamReader(@"Verkocht\euros.txt");
            var sw = new StreamWriter(tempFile);
            string line = sr.ReadLine();
            for (int i = 0; i < alless; i++)
            {
                if (line.Contains(zoeken))
                {
                    sw.WriteLine(line);
                }
            }
            sr.Close();
            sw.Close();
            File.Delete(@"Verkocht\euros.txt");
            File.Move(tempFile, @"Verkocht\euros.txt");
            euroload();
            Savecsv();

        }

        private void btn_verkoop_schoen_Click(object sender, EventArgs e)
        {
            string a = "";
            int aantal = 0;
            int id = 0;
            if (view_csv.SelectedCells.Count > 0)
            {
                int selectedrowindex = view_csv.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = view_csv.Rows[selectedrowindex];
                a = Convert.ToString(selectedRow.Cells["euro"].Value);
                aantal = Convert.ToInt32(selectedRow.Cells["aantal"].Value);
                id = Convert.ToInt32(selectedRow.Cells["id"].Value);

                if (num_verkoop.Value <= aantal)
                {
                    int alles = aantal - Convert.ToInt32(num_verkoop.Value);
                    selectedRow.Cells["aantal"].Value = alles;
                    List<int> euros = new List<int>();
                    StreamWriter sw = new StreamWriter(@"Verkocht\euros.txt", true);
                    StreamWriter sww = new StreamWriter(@"Verkocht\artikelen.csv", true);
                    for (int i = 0; i < num_verkoop.Value; i++)
                    {
                        sw.WriteLine(a);
                        sww.WriteLine(a + ',' + id);
                    }
                    sw.Close();
                    sww.Close();
                    StreamReader sr = new StreamReader(@"Verkocht\euros.txt");
                    while (!sr.EndOfStream)
                    {
                        //, weghalen
                        string[] line = sr.ReadLine().Split();
                        int euro = Convert.ToInt32(line[0]);
                        euros.Add(euro);


                    }
                    sr.Close();
                    int totaal = euros.Sum();
                    lbl_euro.Text = " totaal euros = €" + totaal.ToString();
                    Savecsv();
                }


            }
        }

        private void btn_reload_euros_Click(object sender, EventArgs e)
        {
            euroload();
        }

        private void btn_reset_euros_Click(object sender, EventArgs e)
        {
            File.Delete(@"Verkocht\euros.txt");
            StreamWriter sw = new StreamWriter(@"Verkocht\euros.txt");
            sw.WriteLine("0");
            sw.Close();
            MessageBox.Show("Reset Klaar", "Succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btn_reset_artikelen_Click(object sender, EventArgs e)
        {
            File.Delete(@"Verkocht\artikelen.csv");
            File.Create(@"Verkocht\artikelen.csv");
            MessageBox.Show("Reset Klaar", "Succesvol", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}